/**
 * Represents a node in the system.
 * Created by Trevor on 13/03/2016.
 */
public class Node {
    private String ip;
    private Integer mainPort;
    private Integer broadcastPort;
    private Integer pingPort;

    public Node(String ip, Integer mainPort, Integer broadcastPort, Integer pingPort) {
        this.ip = ip;
        this.mainPort = mainPort;
        this.broadcastPort = broadcastPort;
        this.pingPort = pingPort;
    }

    public boolean isEqual(Node node){
        int broadcast = node.getBroadcastPort();
        int main = node.getMainPort();
        String ip = node.getIp();
        if(broadcast == this.getBroadcastPort() && main == this.getMainPort() && ip.equals(this.getIp()) ) {
            return true;
        }else{
            return false;
        }
    }
    /**
     * Gets the IP of the node.
     * @return The IP of the node.
     */
    public String getIp() {
        return this.ip;
    }

    /**
     * Gets the main input port of the node.
     * @return The main input port.
     */
    public Integer getMainPort() {
        return this.mainPort;
    }

    /**
     * Gets the broadcast input port of the node.
     * @return The broadcast input port.
     */
    public Integer getBroadcastPort() {
        return this.broadcastPort;
    }


    /**
     * Gets the broadcast input port of the node.
     * @return The broadcast input port.
     */
    public Integer getPingPort() {
        return this.pingPort;
    }

    /**
     * Sets the IP of the node.
     * @param ip The IP of the node.
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * Sets the main port of the node.
     * @param port The main port of the node.
     */
    public void setMainPort(Integer port) {
        this.mainPort = port;
    }

    /**
     * Sets the broadcast port of the node.
     * @param port The broadcast port of the node.
     */
    public void setBroadcastPort(Integer port) {
        this.broadcastPort = port;
    }

    /**
     * Sets the broadcast port of the node.
     * @param port The broadcast port of the node.
     */
    public void setPingPort(Integer port) {
        this.pingPort = port;
    }
}
