import java.io.File;
import java.io.IOException;

public class Main {
    public static final Integer MINIMUM_PORT_NUMBER = 10000;
    public static Node myself;
    public static Boolean printDebug = false;
    public static Double memWarnPercent = 0.001;
    public static Integer cacheSize = 100;
    public static Integer numThreads = 50;
    public static String serverListFileName = "servers.txt";
    public static File serverListFile;

    public static void main(String[] args) {
        // Get command line args
        if( args.length < 2 ) {
            Main.printUsageMessage( "incorrect number of arguments" );
            return;
        }

        // Get the IP of the current node.
        String ip;
        try {
            ip = RouteManager.getMyIP();
        } catch(IOException e) {
            System.out.println("Could not determine the IP of the current node. Please try another node.");
            return;
        }

        // Check port number
        Integer mainPortNumber;
        try {
            mainPortNumber = Integer.parseInt( args[0] );
        } catch ( NumberFormatException e ) {
            Main.printUsageMessage( "main port number must be an integer " +
                    "value greater than 10000" );
            return;
        }

        // Check if port number is greater than the minimum port number
        if( mainPortNumber <= Main.MINIMUM_PORT_NUMBER ) {
            Main.printUsageMessage( "main port number must be an integer " +
                    "value greater than 10000" );
            return;
        }

        Integer broadcastPortNumber;
        try {
            broadcastPortNumber = Integer.parseInt( args[1] );
        } catch ( NumberFormatException e ) {
            Main.printUsageMessage( "broadcast port number must be an integer " +
                    "value greater than 10000" );
            return;
        }

        // Check if port number is greater than the minimum port number
        if( broadcastPortNumber <= Main.MINIMUM_PORT_NUMBER ) {
            Main.printUsageMessage( "broadcast port number must be an integer " +
                    "value greater than 10000" );
            return;
        }

        Integer pingPortNumber;
        try {
            pingPortNumber = Integer.parseInt( args[2] );
        } catch ( NumberFormatException e ) {
            Main.printUsageMessage( "ping port number must be an integer " +
                    "value greater than 10000" );
            return;
        }

        // Check if port number is greater than the minimum port number
        if( pingPortNumber <= Main.MINIMUM_PORT_NUMBER ) {
            Main.printUsageMessage( "broadcast port number must be an integer " +
                    "value greater than 10000" );
            return;
        }

        // Create the node data
        System.out.println("IP: " + ip + " Main Port: " + mainPortNumber + " Broadcast Port:" +
                broadcastPortNumber);
        Main.myself = new Node(ip, mainPortNumber, broadcastPortNumber, pingPortNumber);

        // Parse the other arguments
        for (int i = 1; i < args.length; ++i) {
            switch (args[i]) {
                case "-c":
                    // Check the cache size parameter
                    try {
                        Main.cacheSize = Integer.parseInt( args[++i] );
                    } catch ( NumberFormatException e ) {
                        Main.printUsageMessage( "cache size was not a positive integer value" );
                        return;
                    }

                    if( Main.cacheSize <= 0 ) {
                        Main.printUsageMessage( "cache size was not a positive integer value" );
                        return;
                    }
                    break;
                case "-t":
                    // Check the number of threads parameter
                    try {
                        Main.numThreads = Integer.parseInt( args[++i] );
                    } catch ( NumberFormatException e ) {
                        Main.printUsageMessage( "number of threads was not an integer value " +
                                "greater than 2" );
                        return;
                    }

                    if( Main.numThreads < 2 ) {
                        Main.printUsageMessage( "number of threads was not an integer value " +
                                "greater than 2");
                        return;
                    }
                    break;
                case "-s":
                    // Read server list and store all the entry in memory
                    Main.serverListFileName = args[++i];
                    break;
                case "-m":
                    // Check if we want to specify a percentage
                    try {
                        Double percentage = Double.parseDouble( args[++i] );
                        if( percentage >= 0 && percentage <= 1 ) {
                            Main.memWarnPercent = percentage;
                        } else {
                            Main.printUsageMessage( "memWarnPercent must be a value between 0 and 1" );
                            return;
                        }
                    } catch ( NumberFormatException e ) {
                        Main.printUsageMessage( "memWarnPercent must be a value between 0 and 1" );
                        return;
                    }
                    break;
                case "-v":
                    // Check if we want to enable logging
                    Main.printDebug = true;
                    break;
                default:
                    break;
            }
        }

        // Try setting up the route manager with the nodes listed in the specified file.
        Main.serverListFile = new File(Main.serverListFileName);
        try {
            new RouteManager(Main.serverListFile);
        } catch (IOException e) {
            Main.printUsageMessage( "Could not read from: " + Main.serverListFileName +
                    ". Maybe file does not exist.");
            return;
        }

        PingManager pManager = new PingManager();
    }

    /**
     * Prints the help/usage message to the console.
     * @param error The error message associated with why we're printing the
     *              usage message.
     */
    public static void printUsageMessage( String error ) {
        if( error != null ) {
            System.out.println( "error: " + error );
        }
        System.out.println( "usage: java -jar A5.jar mainPortNumber broadcastPort [-c cacheSize] [-t numThreads] " +
                "[-s serverListFile] [-m memWarnPercent] [-v]" );
        System.out.println( "\tmainPortNumber:\t\tThe port number to run the server on.");
        System.out.println( "\tbroadcastPort:\t\tThe port number to broadcast messages regarding dead nodes.");
        System.out.println( "\t-c cacheSize:\t\tThe number of items to store in the cache (default 100).");
        System.out.println( "\t-t numThreads:\t\tThe number of threads the server should use (default 1000).");
        System.out.println( "\t-s serverListFile:\tFile containing the list of known nodes in the network (default \"server.list\").");
        System.out.println( "\t-m memWarnPercent:\tPercentage of memory to give a warning message at." +
                "Value must be between 0 and 1.");
        System.out.println( "\t\t\t\t\t\t(default 0.0001)." );
        System.out.println( "\t-v:\t\t\t\t\tIf added prints out messages for debugging.");
    }
}
