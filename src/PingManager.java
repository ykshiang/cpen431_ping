/**
 * Created by KangShiang on 2016-03-15.
 */
import java.net.*;
import java.nio.*;
import java.util.ArrayList;
import java.util.*;

public class PingManager {

    /*
   PingManager updates the neighbouring nodes by sending the list of dead nodes that it is currently aware of.
   At the same time, it checks every 10 second to see if any of its neighbouring nodes is dead. If ping manager
   hasn't seen the ping message from a now for more than 10 seconds, it set the node to null and uses machine gun to
   broadcast the news to every other nodes.

   Ping message structure:
   ______________________________________________________________________________________________________________
  |                     |               |               |               |
  |   int value:        |   index of    |   index of    |   index of    |
  |                     |               |               |               |   so on.......
  |  Size of the array  |   deadnode 1  |   deadnode 2  |   deadnode 3  |
  |_____________________|_______________|_______________|_______________|________________________________________

     */
    // NOTE: This is only assuming that we have 1000 nodes in our system. System breaks if number of node
    // exceeds 1000!
    private static final int PACKETSIZE = 4096;
    private final int SIZEOFINT = 4;
    private long leftNeighbourLastPing;
    private long rightNeighbourLastPing;
    private Node leftNeighbour;
    private Node rightNeighbour;
    private final long TIMEOUT = 9000;
    private final long PINGFEQ = 3000;

    public PingManager(){
        this.rightNeighbour = RouteManager.getRightNeighbour();
        this.leftNeighbour = RouteManager.getLeftNeighbour();

        System.out.println("rightNeighbour " + rightNeighbour.getMainPort());
        System.out.println("leftNeighbour " + leftNeighbour.getMainPort());
        PrintLog.log(this.getClass(), "Constructing Ping Manager");
        setupPingClient();
        setupPingServer();
        try {
            Thread.sleep(TIMEOUT);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        setupStatusChecker();
    }

    private int extractPort(byte[] data){
        byte[] portData = new byte[SIZEOFINT];
        System.arraycopy(data, 0, portData, 0, SIZEOFINT);
        ByteBuffer bb = ByteBuffer.wrap(portData);
        int port = bb.getInt();
        return port;
    }

    private int[] unpackData(byte[] data){
        ArrayList<Integer> deadNodes = new ArrayList<Integer>();
        int size;
        int offset = 0;
        byte[] sizeData = new byte[SIZEOFINT];
        System.arraycopy(data, SIZEOFINT, sizeData, 0, SIZEOFINT);
        offset = SIZEOFINT + SIZEOFINT;
        ByteBuffer bb = ByteBuffer.wrap(sizeData);
        size = bb.getInt();

        for(int i = 0; i < size; i++){
            byte[] deadNodeData = new byte[SIZEOFINT];
            System.arraycopy(data, offset, deadNodeData, 0, SIZEOFINT);
            offset = offset + SIZEOFINT;
            ByteBuffer buffer = ByteBuffer.wrap(deadNodeData);
            int index = buffer.getInt();
            deadNodes.add(new Integer(index));
        }

        int[] deadNodesIndexes = new int[deadNodes.size()];

        for(int i = 0; i < deadNodes.size(); i++){
            deadNodesIndexes[i] = deadNodes.get(i);
        }

        return deadNodesIndexes;

    }

    private byte[] packData(int[] data){
        int size = data.length;
        int offset = 0;
        byte[] portData = ByteBuffer.allocate(SIZEOFINT).putInt(Main.myself.getMainPort().intValue()).array();
        byte[] sizeData = ByteBuffer.allocate(SIZEOFINT).putInt(size).array();
        byte[] contentData = new byte[size * SIZEOFINT];

        for(int deadindex : data){
            byte[] intData = ByteBuffer.allocate(SIZEOFINT).putInt(deadindex).array();
            System.arraycopy(intData, 0, contentData, offset, SIZEOFINT);
            offset = offset + SIZEOFINT;
        }

        byte[] packet = new byte[portData.length + sizeData.length + contentData.length];
        System.arraycopy(portData, 0 ,packet, 0 , portData.length);
        System.arraycopy(sizeData, 0, packet, portData.length, sizeData.length);
        System.arraycopy(contentData,0,packet,portData.length + sizeData.length, contentData.length);
        return packet;
    }

    private void pingNode(Node node) throws Exception{
        System.out.println("Pinging node " + node.getMainPort());
        String nodeIP = node.getIp();
        Integer nodePort = node.getPingPort();
        DatagramSocket clientSocket = new DatagramSocket();
        InetAddress IPAddress = InetAddress.getByName(nodeIP);
        int[] deadNodes = RouteManager.getAllDeadNodes();
        byte[] sendData = packData(deadNodes);
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, nodePort);
        clientSocket.send(sendPacket);
        clientSocket.close();
    }

    private void setupPingClient(){
        Thread client = new Thread(new Runnable() {
            public void run(){
                while(true){
                    try {
                        System.out.println("Ping rightNeighbour");
                        pingNode(rightNeighbour);
                        System.out.println("Ping leftNeighbour");
                        pingNode(leftNeighbour);
                        Thread.sleep(PINGFEQ);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        client.start();
    }

    private void setupPingServer(){
        Thread server = new Thread(new Runnable() {
            public void run(){
                try
                {
                    // Construct the socket
                    DatagramSocket socket = new DatagramSocket( Main.myself.getPingPort()) ;
                    System.out.println( "The server is ready..." ) ;
                    while(true)
                    {
                        DatagramPacket packet = new DatagramPacket( new byte[PACKETSIZE], PACKETSIZE );
                        socket.receive(packet) ;
                        InetAddress addr = packet.getAddress();

                        int port = extractPort(packet.getData());
                        System.out.println("Sender Port number is " + port);
                        if(addr.getHostAddress().equals(leftNeighbour.getIp()) && port == leftNeighbour.getMainPort().intValue()){
                            Date date = new Date();
                            leftNeighbourLastPing = date.getTime();
                        }

                        if(addr.getHostAddress().equals(rightNeighbour.getIp()) && port == rightNeighbour.getMainPort().intValue()){
                            Date date = new Date();
                            rightNeighbourLastPing = date.getTime();
                        }

                        // TODO: Compare the list with our RouteManager.serverlist
                        int [] deadNodes = unpackData(packet.getData());
                        for (int i : deadNodes){
                            System.out.println(i);
                        }
                    }
                }
                catch( Exception e )
                {
                    System.out.println( e ) ;
                }
            }
        });
        server.start();
    }

    private void setupStatusChecker(){
        Thread server = new Thread(new Runnable() {
            public void run(){
                while(true) {
                    Date date = new Date();
                    long currentTime = date.getTime();

                    if (currentTime > leftNeighbourLastPing + TIMEOUT) {
                        System.out.println("leftNeighbour expires!");
                        RouteManager.broadcastDeadNode(RouteManager.getDestinationNodeIndex(leftNeighbour.getIp(), leftNeighbour.getBroadcastPort()));
                        RouteManager.removeDeadNode(leftNeighbour.getIp(), leftNeighbour.getBroadcastPort());
                        System.out.println(leftNeighbour.getIp() + " " + leftNeighbour.getMainPort() + " is dead");
                        leftNeighbour = RouteManager.getLeftNeighbour();
                    }

                    if (currentTime > rightNeighbourLastPing + TIMEOUT) {
                        System.out.println("rightNeighbour expires!");
                        RouteManager.broadcastDeadNode(RouteManager.getDestinationNodeIndex(rightNeighbour.getIp(), rightNeighbour.getBroadcastPort()));
                        RouteManager.removeDeadNode(rightNeighbour.getIp(), rightNeighbour.getBroadcastPort());
                        System.out.println(rightNeighbour.getIp() + " " + rightNeighbour.getMainPort() + " is dead");
                        rightNeighbour = RouteManager.getRightNeighbour();

                    }

                    try {
                        Thread.sleep(TIMEOUT);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        server.start();
    }
}
