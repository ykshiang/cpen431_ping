import java.io.File;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.net.*;


public class RouteManager {
    // TODO: Check if serverList is a good data type for storing nodes
    private static List<Node> serverList;
    //private static ArrayList<Node> serverList;
    private static final String SECRET = "Sd90sX";
    private static final String RELIABLE_SOURCE = "http://checkip.amazonaws.com";
    private static final String LOCALHOST = "127.0.0.1";

    private static int getAbsIndex(int index){
        int d = Math.abs(index)/serverList.size();
        if(index>=0){
            return((index) % serverList.size());
        }else{
            return ((serverList.size() * (d+1)) + index);
        }
    }

    private static int traverseRight(int index){
        int absIndex = getAbsIndex(index);
        if(absIndex == serverList.size() - 1){
            return 0;
        }else{
            return absIndex + 1;
        }
    }

    private static int traverseLeft(int index){
        int absIndex = getAbsIndex(index);
        if(absIndex == 0){
            return serverList.size() - 1;
        }else{
            return absIndex - 1;
        }
    }

    public static Node getLeftNeighbour(){
        int myIndex = getDestinationNodeIndex(Main.myself.getIp(), Main.myself.getBroadcastPort());
        System.out.println("Current Index is " + myIndex);
        int previous = traverseLeft(myIndex);
        System.out.println("Previous Index is " + previous);
        while(serverList.get(previous)==null){
            previous = traverseLeft(previous);
            System.out.println("Previous Index is " + previous);
        }
        System.out.println("Getting server at previous index : " + previous);
        Node neighbour = serverList.get(previous);
        return neighbour;
    }

    public static Node getRightNeighbour(){
        int myIndex = getDestinationNodeIndex(Main.myself.getIp(), Main.myself.getBroadcastPort());
        int next = traverseRight(myIndex);
        while(serverList.get(next) == null){
            next = traverseRight(next);
        }
        Node neighbour = serverList.get(next);
        return neighbour;
    }

    public static int[] getAllDeadNodes(){
        ArrayList<Integer> deadNodes = new ArrayList<Integer>();

        for( int i = 0; i < serverList.size(); i++ ){
            if(serverList.get(i) == null){
                deadNodes.add(i);
            }
        }

        System.out.println("Number of deadnode : " + deadNodes.size());
        if(deadNodes.size() > 0) {
            int[] deadNodesIndexes = new int[deadNodes.size()];

            for (int i = 0; i < deadNodes.size(); i++) {
                deadNodesIndexes[i] = deadNodes.get(i);
            }

            return deadNodesIndexes;
        }else{
            return new int[0];
        }
    }

    public RouteManager(File serverListFile) throws IOException {
        serverList = Collections.synchronizedList(new ArrayList<Node>());

        // Read server list
        readFile(serverListFile);

        try {
            setupDeadNodeReceiverPort();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the node's current IP against a "reliable" online source.
     * @throws IOException
     */
    public static String getMyIP() throws IOException {
        String ip;
        try {
            URL whatismyip = new URL(RouteManager.RELIABLE_SOURCE);
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    whatismyip.openStream()));
            ip = in.readLine();
        } catch(IOException e) {
            PrintLog.log(RouteManager.class,"Had issues retrieving IP from reliable source. Returning localhost.");
            ip = RouteManager.LOCALHOST;
        }

        return ip;
    }

    /**
     * Looks up an IP associated with a particular domain name.
     * @param domain The domain to lookup.
     * @return The IP address associated with the domain name.
     */
    private static String ipLookup(String domain){
        // Handle special case when the user specifies localhost.
        if(domain.equals(RouteManager.LOCALHOST)) {
            try {
                return RouteManager.getMyIP();
            } catch(IOException e) {
                return null;
            }
        }

        String ipAddress;
        try {
            InetAddress inetAddress = InetAddress.getByName(domain);
            ipAddress = inetAddress.getHostAddress();
        } catch (UnknownHostException e) {
            System.out.println("Could not parse domain name: " + domain);
            return null;
        }
        return ipAddress;
    }

    /**
     * Reads the lines in a file that contains the IPs and ports of the other nodes in the
     * system.
     * @param list The file containing the IPs and ports of the other nodes.
     * @throws IOException
     */
    public static void readFile(File list) throws IOException {
        FileInputStream fis = new FileInputStream(list);

        //Construct BufferedReader from InputStreamReader
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));

        // Read every line in the file
        String line;
        while ((line = br.readLine()) != null) {
            String[] ipPort = line.split(":");

            // Format of each line in the file should be: IP:MainInputPort:BroadcastPort
            // IP is the IP of the node
            // MainInputPort is the port the node will receive all incoming requests/replies
            // BroadcastPort is the port the node will receive all route manager related info
            if( ipPort.length < 3 ) {
                PrintLog.log(RouteManager.class,"Could not parse the line: " + line);
                continue;
            }

            // Set the IP and port values
            String ip = ipLookup(ipPort[0]);
            Integer mainPort = Integer.valueOf(ipPort[1]);
            Integer broadcastPort = Integer.valueOf(ipPort[2]);
            Integer pingPort = Integer.valueOf(ipPort[3]);
            if (ip == null || mainPort == null || broadcastPort == null || pingPort == null) {
                PrintLog.log(RouteManager.class,"Could not parse the line: " + line);
                continue;
            } else {
                PrintLog.log(RouteManager.class, "Parsed line: IP: " + ip + " Main Port: " + mainPort +
                        " Broadcast Port: " + broadcastPort + " Ping Port: " + pingPort);
            }

            // Add the node to the serverList
            Node node = new Node(ip, mainPort, broadcastPort, pingPort);
            serverList.add(node);
        }
        br.close();
    }

    /**
     * Removes a dead node from the list of all nodes.
     * @param ipAddress The IP address of the node to remove.
     */
    public static void removeDeadNode(String ipAddress, Integer portNumber){
        final Integer index = RouteManager.getDestinationNodeIndex(ipAddress, portNumber);
        PrintLog.log(RouteManager.class,"Removing node at index: " + index + " (" + ipAddress + ")");

        if(index.intValue() > 0 && index.intValue() < serverList.size()) {
            serverList.set(index.intValue(), null);

            Runnable deadBroadcast = new Runnable() {
                public void run() {
                    broadcastDeadNode(index);
                }
            };
            new Thread(deadBroadcast).start();
        }else{
            PrintLog.log(RouteManager.class, "No Node Removed, Error, index out of bounds" + index);
        }
        //System.out.println("Server " + serverList.get(index.intValue()).getMainPort() + " removed");
        // Starts a thread to start broadcasting the dead node to other node.
    }

    /**
     * Gets the index of the destination node in the list of server nodes.
     * @param ipAddress The IP address of the destination node.
     * @param portNumber The port number of the destination node.
     * @return The index of the IP in the list of server nodes.
     */
    // TODO: This does not account for different broadcasr port and different ping port
    public static Integer getDestinationNodeIndexWithMainPort(String ipAddress, Integer portNumber) {
        // Get the IP address index
        Integer ipIndex = 0;
        for( Node node : serverList ) {
            if(ipAddress.equals(node.getIp())) {
                if(portNumber.equals(node.getMainPort())) {
                    break;
                }
            }
            ipIndex++;
        }

        return ipIndex;
    }

    /**
     * Gets the index of the destination node in the list of server nodes.
     * @param ipAddress The IP address of the destination node.
     * @param portNumber The port number of the destination node.
     * @return The index of the IP in the list of server nodes.
     */
    // TODO: This does not account for different broadcasr port and different ping port
    public static Integer getDestinationNodeIndex(String ipAddress, Integer portNumber) {
        // Get the IP address index
        try {
            Integer ipIndex = 0;
            boolean ifFound = false;
            for (Node node : serverList) {
                if(node != null) {
                    if (ipAddress.equals(node.getIp())) {
                        if (portNumber.equals(node.getBroadcastPort())) {
                            ifFound = true;
                            break;
                        }
                    }
                }
                ipIndex++;
            }

            if(ifFound){
                System.out.println("Node Index was found at " + ipIndex);
                return ipIndex;
            }else{
                System.out.println("Node Index was found at " + -1);
                return -1;
            }
        }catch(IndexOutOfBoundsException e){
            return -1;
        }catch (NullPointerException e){
            return -1;
        }
    }

    /**
     * Gets the destination node responsible for handling a specified key.
     * @param key The key to handle.
     * @return The node responsible for handling the key.
     */
    public static Node getDestinationNode(byte[] key){
        int code = Arrays.hashCode(key);
        code = Math.abs(code);

        PrintLog.log(RouteManager.class,"Hashcode: " + code);

        // TODO: Maybe change this from a modulus to something faster
        int index = code % serverList.size();
        Node node = serverList.get(index);
        PrintLog.log(RouteManager.class,"Hashcode index: " + index + ", Server List Size: " + serverList.size());
        PrintLog.log(RouteManager.class,"Node at index: " + node.getIp() + ", Main Port: " +
                node.getMainPort() + ", Broadcast Port: " + node.getMainPort());
        return node;
    }

    /**
     * Sets up the port for receiving messages regarding dead nodes.
     * @throws Exception
     */
    private void setupDeadNodeReceiverPort()throws Exception{
        Thread receiver = new Thread(new Runnable() {
            public void run() {
                DatagramSocket serverSocket = null;
                try {
                    serverSocket = new DatagramSocket(Main.myself.getBroadcastPort());
                } catch (SocketException e) {
                    e.printStackTrace();
                }

                if( serverSocket == null ) {
                    PrintLog.log(this.getClass(), "Could not set up a server socket at port: " +
                            Main.myself.getBroadcastPort() );
                    return;
                }

                // Continually read in received packets.
                while(true) {
                    // TODO: Fix so that it can handle packets larger than 1024 bytes
                    byte[] receiveData = new byte[1024];

                    DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                    try {
                        serverSocket.receive(receivePacket);
                        // Get the values from the packet
                        int currentIndex = 0;
                        int secretLength = SECRET.getBytes().length;
                        byte[] packetData = receivePacket.getData();
                        byte[] secret = Arrays.copyOfRange( packetData, currentIndex, secretLength );
                        currentIndex = secretLength;
                        byte[] nodeIndexByteArray = Arrays.copyOfRange( packetData, currentIndex,
                                currentIndex + 4 );
                        System.out.println("Received Broadcase Message. Checking if secret matches.");
                        // Check if the secret value matches the hardcoded secret value.
                        // This check was added to ensure we don't read in any bad packets.
                        if(Arrays.equals(secret, SECRET.getBytes())) {
                            int nodeIndex = ByteBuffer.wrap(nodeIndexByteArray).getInt();

                            PrintLog.log(this.getClass(),"Removing index: " + nodeIndex);

                            // Removes the index from the list by setting the index to null.
                            serverList.set(nodeIndex, null);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        break;
                    }
                }
            }
        });
        receiver.start();
    }

    /**
     * Creates a packet to send to other nodes consisting of a dead node's index and a secret key.
     * @param deadNodeIndex The index of the dead node.
     */
    public static void broadcastDeadNode(int deadNodeIndex){

        if(deadNodeIndex == -1){
            return;
        }
        // Create the packet to send
        byte[] nodeIndex = ByteBuffer.allocate(4).putInt(deadNodeIndex).array();
        byte[] secret = SECRET.getBytes();

        byte[] packaged = new byte[nodeIndex.length + secret.length];
        ByteBuffer pack = ByteBuffer.wrap(packaged);
        pack.put(secret);
        pack.put(nodeIndex);

        // Create socket to broadcast from
        DatagramSocket datagramSocket = null;
        try {
            datagramSocket = new DatagramSocket();
        }catch(SocketException e){
            e.printStackTrace();
        }

        if( datagramSocket == null ) {
            PrintLog.log(RouteManager.class, "Could not create socket to broadcast out." );
            return;
        }

        DatagramPacket packet;
        InetAddress destIp = null;
        // Go through all the IPs and ports in the server list and send the dead node to
        // all the nodes.
        for(Node node : serverList) {
            try {
                String ip = node.getIp();
                try {
                    destIp = InetAddress.getByName(ip);
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                // Create the packet to send
                Integer destPort = node.getBroadcastPort();
                System.out.println("Broadcasting to " + ip + " at " + destPort);
                packet = new DatagramPacket(packaged, packaged.length, destIp, destPort);

                try {
                    datagramSocket.send(packet);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }catch(NullPointerException e){

            }
        }
    }
}
