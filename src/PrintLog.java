public class PrintLog {
    public static void log(Class callingClass) {
        if(Main.printDebug) {
            Long time = System.nanoTime();
            System.out.println(time + ": " + callingClass.getCanonicalName());
        }
    }

    public static void log(Class callingClass, boolean x) {
        if(Main.printDebug) {
            Long time = System.nanoTime();
            System.out.println(time + ": " + callingClass.getCanonicalName() + ": " + x);
        }
    }

    public static void log(Class callingClass, char x) {
        if(Main.printDebug) {
            Long time = System.nanoTime();
            System.out.println(time + ": " + callingClass.getCanonicalName() + ": " + x);
        }
    }


    public static void log(Class callingClass, int x) {
        if(Main.printDebug) {
            Long time = System.nanoTime();
            System.out.println(time + ": " + callingClass.getCanonicalName() + ": " + x);
        }
    }


    public static void log(Class callingClass, long x) {
        if(Main.printDebug) {
            Long time = System.nanoTime();
            System.out.println(time + ": " + callingClass.getCanonicalName() + ": " + x);
        }
    }


    public static void log(Class callingClass, float x) {
        if(Main.printDebug) {
            Long time = System.nanoTime();
            System.out.println(time + ": " + callingClass.getCanonicalName() + ": " + x);
        }
    }


    public static void log(Class callingClass, double x) {
        if(Main.printDebug) {
            Long time = System.nanoTime();
            System.out.println(time + ": " + callingClass.getCanonicalName() + ": " + x);
        }
    }


    public static void log(Class callingClass, char x[]) {
        if(Main.printDebug) {
            Long time = System.nanoTime();
            System.out.println(time + ": " + callingClass.getCanonicalName() + ": " + x);
        }
    }


    public static void log(Class callingClass, String x) {
        if(Main.printDebug) {
            Long time = System.nanoTime();
            System.out.println(time + ": " + callingClass.getCanonicalName() + ": " + x);
        }
    }


    public static void log(Class callingClass, Object x) {
        if(Main.printDebug) {
            Long time = System.nanoTime();
            String s = String.valueOf(x);
            System.out.println(time + ": " + callingClass.getCanonicalName() + ": " + x);
        }
    }
}