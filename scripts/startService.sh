#!/usr/bin/bash
shopt -s expand_aliases
# Runs all the setup logic for a Linux server
# Designed to be used for PlanetLab nodes

# Remote path where the packages should exist
PACKAGES_PATH=/home/ubc_cpen431_3/group_3a/a5
echo "Killing the previously running server: "$HOSTNAME

pkill -f a5group3a.jar

sleep 5

echo "Running script for: "$HOSTNAME

nohup java -jar -Xmx64m -XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled $PACKAGES_PATH/a5group3a.jar 63123 -s $PACKAGES_PATH/a5_servers.txt > log.txt &

echo "Done Running script for: "$HOSTNAME
