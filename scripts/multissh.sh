# SSH's into a list of nodes
# Designed to be used for PlanetLab nodes
USER=ubc_cpen431_3

# A file containing a list of servers to copy the file to
SERVER_LIST=$1

# The script to run after SSH'ing
INIT_SCRIPT=$2

if [ ! -e $SERVER_LIST ]
then
    echo "Server list file does not exist"
    echo "usage: ./multissh.sh serverListFile [initScript]"
    exit 1
fi

if [ ! -e $INIT_SCRIPT ]
then
    echo "Init script file does not exist"
    echo "usage: ./multissh.sh serverListFile [initScript]"
    exit 1
fi

# SSH to the different servers and run the init script if specified
while read p; do
    if [ -z "$INIT_SCRIPT" ]
    then
        sudo ssh -v $USER@$p < /dev/null
    else
        sudo ssh -q -o "BatchMode=yes" -o "ConnectTimeout=10" $USER@$p 'sudo -S -v && bash -s' < $INIT_SCRIPT &
    fi
done <$SERVER_LIST

wait
