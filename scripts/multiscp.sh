#!/bin/bash
# Runs SCP for a particular file to a number of specified servers.

LOGIN=ubc_cpen431_3
REMOTE_PATH=/home/ubc_cpen431_3/group_3a/a5/

# The identity file
IDENTITY=$1

# The file to send to the servers
FILE_TO_SEND=$2

# A file containing a list of servers to copy the file to
SERVER_LIST=$3

# Check if the identity file exists
if [ ! -e $IDENTITY ]
then
    echo "Identity file does not exist"
    echo "usage: ./multiscp.sh identityFile fileToSend serverListFile"
    exit 1
fi

# Check if the specified files exist
if [ ! -e $FILE_TO_SEND ]
then
    echo "File to send does not exist"
    echo "usage: ./multiscp.sh identityFile fileToSend serverListFile"
    exit 1
fi

if [ ! -e $SERVER_LIST ]
then
    echo "Server list file does not exist"
    echo "usage: ./multiscp.sh identityFile fileToSend serverListFile"
    exit 1
fi

# Send the file to all the specified servers
while read p; do
    # output="$(scp -r -o StrictHostKeyChecking=no -o ConnectTimeout=10 -i $IDENTITY $FILE_TO_SEND $LOGIN@$p:$REMOTE_PATH)"
    output=$(scp -r -o StrictHostKeyChecking=no -o ConnectTimeout=10 -i $IDENTITY $FILE_TO_SEND $LOGIN@$p:$REMOTE_PATH)

    echo "file(s) sent to $p"
done <$SERVER_LIST
