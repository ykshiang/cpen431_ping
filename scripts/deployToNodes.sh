PRIVATE_KEY=$1
if [ $# -eq 0 ]
	then
		echo "Please provide your private key as a parameter"
		echo ""
		echo "Example:"
		echo "	./deployToNodes.sh id_rsa"
		echo ""
else

	echo "mkdir first..."
	sudo ./multissh.sh a5_servers.txt setupDir.sh

	sed 's/$/:63123/' a5_servers.txt > servers.txt
	echo "Copying server list with port number to testjar folder..."
    cp servers.txt ../testjars/testNodes.list
	echo "Copying servers.txt to Nodes:"
	sudo ./multiscp.sh $PRIVATE_KEY ./a5_servers.txt a5_servers.txt

	echo " "
	echo " "
	echo "Copying jar file to Nodes:"
    cp ../A5.jar ./a5group3a.jar
	sudo ./multiscp.sh $PRIVATE_KEY ./a5group3a.jar a5_servers.txt

	echo " "
	echo " "
	echo "Restarting services on Nodes:"
	sudo ./multissh.sh a5_servers.txt startService.sh
fi
